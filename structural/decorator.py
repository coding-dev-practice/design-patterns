class Coffee:
    def cost(self) -> int:
        return 5

    def description(self) -> str:
        return "Coffee"


class CoffeeDecorator(Coffee):
    def __init__(self, coffee) -> None:
        self.decorated_coffee: Coffee = coffee

    def cost(self) -> int:
        return self.decorated_coffee.cost()

    def description(self) -> str:
        return self.decorated_coffee.description()


class Milk(CoffeeDecorator):
    def cost(self) -> int:
        return self.decorated_coffee.cost() + 2

    def description(self) -> str:
        return self.decorated_coffee.description() + ", milk"


class Sugar(CoffeeDecorator):
    def cost(self) -> int:
        return self.decorated_coffee.cost() + 1

    def description(self) -> str:
        return self.decorated_coffee.description() + ", sugar"


class WhippedCream(CoffeeDecorator):
    def cost(self) -> int:
        return self.decorated_coffee.cost() + 3

    def description(self) -> str:
        return self.decorated_coffee.description() + ", whipped cream"


if __name__ == "__main__":
    basic_coffee = Coffee()
    coffee_with_milk = Milk(basic_coffee)
    supreme_coffee = WhippedCream(Sugar(Milk(basic_coffee)))

    print(f"{basic_coffee.description()}: ${basic_coffee.cost()}")
    print(f"{coffee_with_milk.description()}: ${coffee_with_milk.cost()}")
    print(f"{supreme_coffee.description()}: ${supreme_coffee.cost()}")
    """
    OUTPUT:
    Coffee: $5
    Coffee, milk: $7
    Coffee, milk, sugar, whipped cream: $11
    """
