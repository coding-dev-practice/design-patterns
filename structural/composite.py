from abc import ABC, abstractmethod


class Graphic(ABC):
    @abstractmethod
    def draw(self) -> None:
        pass

    @abstractmethod
    def move(self, x: int, y: int) -> None:
        pass


class Dot(Graphic):
    def __init__(self, x: int, y: int) -> None:
        self.x = x
        self.y = y

    def draw(self) -> None:
        print(f"Drawing a dot at ({self.x},{self.y})")

    def move(self, x: int, y: int) -> None:
        self.x += x
        self.y += y


class Circle(Dot):
    def __init__(self, x: int, y: int, r: int) -> None:
        super().__init__(x, y)
        self.r = r

    def draw(self) -> None:
        print(f"Drawing a circle with radius {self.r} at ({self.x},{self.y})")


class CompositeGraphic:
    def __init__(self) -> None:
        self.objects: list[Graphic] = []

    def add(self, graphic_object) -> None:
        self.objects.append(graphic_object)

    def move(self, x, y) -> None:
        for graphic_object in self.objects:
            graphic_object.move(x, y)

    def draw(self) -> None:
        for graphic_object in self.objects:
            graphic_object.draw()


if __name__ == "__main__":
    composite_graphic = CompositeGraphic()
    composite_graphic.add(Circle(0, 0, 3))
    composite_graphic.add(Circle(-1, 2, 1))
    composite_graphic.add(Dot(1, 4))
    composite_graphic.add(Dot(0, 1))
    composite_graphic.draw()
    print("==============")
    composite_graphic.move(10, 0)
    composite_graphic.draw()
    """
    OUTPUT:
    Drawing a circle with radius 3 at (0,0)
    Drawing a circle with radius 1 at (-1,2)
    Drawing a dot at (1,4)
    Drawing a dot at (0,1)
    ==============
    Drawing a circle with radius 3 at (10,0)
    Drawing a circle with radius 1 at (9,2)
    Drawing a dot at (11,4)
    Drawing a dot at (10,1)
    """
