class User:
    def __init__(self, admin_rights: bool) -> None:
        self.admin: bool = admin_rights


class Database:
    def get_data(self) -> None:
        print("Giving all the data from the database")


class Proxy:
    def __init__(self, db: Database) -> None:
        self.db = db

    def get_data_from_db(self, user: User) -> None:
        if user.admin:
            self.db.get_data()
        else:
            print("Access denied - no admin rights")


if __name__ == "__main__":
    admin = User(admin_rights=True)
    normal_user = User(admin_rights=False)
    proxy = Proxy(Database())
    proxy.get_data_from_db(admin)
    proxy.get_data_from_db(normal_user)
    """
    OUTPUT:
    Giving all the data from the database
    Access denied - no admin rights
    """
