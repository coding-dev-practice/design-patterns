from __future__ import annotations
from abc import ABC, abstractmethod


class Shape:
    def __init__(self, color: Color) -> None:
        self.color: Color = color

    def draw(self) -> None:
        pass


class Color(ABC):
    @abstractmethod
    def fill_with_color(self, shape: Shape) -> None:
        pass


class Circle(Shape):
    def draw(self) -> None:
        self.color.fill_with_color("circle")


class Square(Shape):
    def draw(self) -> None:
        self.color.fill_with_color("square")


class Red(Color):
    def fill_with_color(self, shape) -> None:
        print(f"Red colored {shape}")


class Blue(Color):
    def fill_with_color(self, shape) -> None:
        print(f"Blue colored {shape}")


if __name__ == "__main__":
    red_circle = Circle(Red())
    blue_square = Square(Blue())
    red_circle.draw()
    blue_square.draw()
    """
    OUTPUT:
    Red colored circle
    Blue colored square
    """
