from __future__ import annotations


class Tree:
    def __init__(self, x: int, y: int, tree_type: TreeType):
        self.x = x
        self.y = y
        self.tree_type = tree_type

    def draw(self):
        self.tree_type.draw(self.x, self.y)


class TreeType:
    def __init__(self, name: str, color: str) -> None:
        self.name = name
        self.color = color

    def draw(self, x: int, y: int):
        print(f"Drawing a {self.color} {self.name} at ({x},{y})")


class TreeTypeManager:
    tree_types: dict[TreeType] = {}

    @staticmethod
    def get_tree_type(name: str, color: str):
        key = (name, color)
        if key not in TreeTypeManager.tree_types:
            TreeTypeManager.tree_types[key] = TreeType(name, color)
        return TreeTypeManager.tree_types[key]


class Forest:
    def __init__(self) -> None:
        self.trees: list[Tree] = []

    def plant_tree(self, x: int, y: int, color: str, name: str):
        tree = Tree(x, y, TreeType(name, color))
        self.trees.append(tree)

    def plant_tree_flyweight(self, x: int, y: int, color: str, name: str):
        tree = Tree(x, y, TreeTypeManager.get_tree_type(name, color))
        self.trees.append(tree)

    def draw(self):
        for tree in self.trees:
            tree.draw()

    def count_unique_tree_types(self):
        return len({id(tree.tree_type) for tree in self.trees})


if __name__ == "__main__":
    forest = Forest()
    forest.plant_tree(1, 2, "brown", "oak")
    forest.plant_tree(3, 4, "white", "pine")
    forest.plant_tree(2, 0, "white", "pine")
    forest.plant_tree(2, 0, "white", "pine")
    forest.plant_tree(2, 0, "white", "pine")
    forest.plant_tree(2, 0, "white", "pine")
    forest.plant_tree(2, 0, "white", "pine")
    forest.plant_tree(2, 0, "white", "pine")
    forest.plant_tree(2, 0, "white", "pine")
    forest.draw()
    print(f"Unique tree types: {forest.count_unique_tree_types()}")

    forest_flyweight = Forest()
    forest_flyweight.plant_tree_flyweight(1, 2, "brown", "oak")
    forest_flyweight.plant_tree_flyweight(3, 4, "white", "pine")
    forest_flyweight.plant_tree_flyweight(2, 0, "white", "pine")
    forest_flyweight.plant_tree_flyweight(2, 0, "white", "pine")
    forest_flyweight.plant_tree_flyweight(2, 0, "white", "pine")
    forest_flyweight.plant_tree_flyweight(2, 0, "white", "pine")
    forest_flyweight.plant_tree_flyweight(2, 0, "white", "pine")
    forest_flyweight.plant_tree_flyweight(2, 0, "white", "pine")
    forest_flyweight.plant_tree_flyweight(2, 0, "white", "pine")
    forest_flyweight.draw()
    print(
        f"Unique tree types for flyweight: {forest_flyweight.count_unique_tree_types()}"
    )
    """
    OUTPUT:
    Drawing a brown oak at (1,2)
    Drawing a white pine at (3,4)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Unique tree types: 9
    Drawing a brown oak at (1,2)
    Drawing a white pine at (3,4)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Drawing a white pine at (2,0)
    Unique tree types for flyweight: 2
    """
