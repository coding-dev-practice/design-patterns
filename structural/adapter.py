class Duck:
    def quack(self) -> None:
        print("Quack")

    def fly(self) -> None:
        print("I'm flying")


class Turkey:
    def gobble(self) -> None:
        print("Gobble gobble")

    def fly(self) -> None:
        print("I'm flying a short distance")


def duck_interaction(duck: Duck) -> None:
    duck.quack()
    duck.fly()


class TurkeyAdapter(Duck):
    def __init__(self, turkey: Turkey) -> None:
        self.turkey = turkey

    def quack(self) -> None:
        self.turkey.gobble()

    def fly(self) -> None:
        for _ in range(5):
            self.turkey.fly()


if __name__ == "__main__":
    duck = Duck()
    turkey = Turkey()
    turkey_adapter = TurkeyAdapter(turkey)

    print("The Turkey says...")
    turkey.gobble()
    turkey.fly()

    print("\nThe Duck says...")
    duck_interaction(duck)

    print("\nThe TurkeyAdapter says...")
    duck_interaction(turkey_adapter)
    """
    OUTPUT:
    The Turkey says...
    Gobble gobble
    I'm flying a short distance

    The Duck says...
    Quack
    I'm flying

    The TurkeyAdapter says...
    Gobble gobble
    I'm flying a short distance
    I'm flying a short distance
    I'm flying a short distance
    I'm flying a short distance
    I'm flying a short distance
    """