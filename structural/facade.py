class CPU:
    def start(self) -> None:
        print("Starting CPU...")

    def turn_off(self) -> None:
        print("Turning off CPU...")


class Memory:
    def initialize(self) -> None:
        print("Memory allocated")

    def clear(self) -> None:
        print("Memory cleared")


class HardDrive:
    def start(self) -> None:
        print("Starting HDD")

    def launch_os(self, system: str) -> None:
        print(f"Starting {system} OS from hard drive")

    def shut_down(self) -> None:
        print("Shutting down HDD")


class ComputerFacade:
    def __init__(self) -> None:
        self.cpu = CPU()
        self.memory = Memory()
        self.harddrive = HardDrive()

    def start(self) -> None:
        self.cpu.start()
        self.memory.initialize()
        self.harddrive.start()
        self.harddrive.launch_os("linux")

    def shutdown(self) -> None:
        self.harddrive.shut_down()
        self.memory.clear()
        self.cpu.turn_off()


if __name__ == "__main__":
    computer = ComputerFacade()
    computer.start()
    print("===============")
    computer.shutdown()
    """
    OUTPUT:
    Starting CPU...
    Memory allocated
    Starting HDD
    Starting linux OS from hard drive
    ===============
    Shutting down HDD
    Memory cleared
    Turning off CPU...
    """
