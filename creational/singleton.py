class TiggerMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class Tigger(metaclass=TiggerMeta):
    def __str__(self) -> str:
        return "I'm the only one!"

    def roar(self) -> str:
        return "Grrr!"


if __name__ == "__main__":
    a = Tigger()
    b = Tigger()

    print(f"ID(a) = {id(a)}")
    print(f"ID(b) = {id(b)}")
    print(f"Are they the same object? {a is b}")
    """
    OUTPUT:
    ID(a) = 139653843597728
    ID(b) = 139653843597728
    Are they the same object? True
    """
