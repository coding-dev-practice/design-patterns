from abc import ABC, abstractmethod


class Button:
    def __init__(self, description: str) -> None:
        self.description = description


class TextBox:
    def __init__(self, description: str) -> None:
        self.description = description


class DarkButton(Button):
    pass


class LightButton(Button):
    pass


class DarkTextBox(TextBox):
    pass


class LightTextBox(TextBox):
    pass


class ThemeFactory(ABC):
    @abstractmethod
    def create_button(self) -> Button:
        pass

    @abstractmethod
    def create_textbox(self) -> TextBox:
        pass


class DarkThemeFactory(ThemeFactory):
    def create_button(self) -> Button:
        return DarkButton("Black button")

    def create_textbox(self) -> TextBox:
        return DarkTextBox("Black textbox")


class LightThemeFactory(ThemeFactory):
    def create_button(self) -> Button:
        return LightButton("White button")

    def create_textbox(self) -> TextBox:
        return LightTextBox("White textbox")


if __name__ == "__main__":
    light_factory = LightThemeFactory()
    dark_factory = DarkThemeFactory()
    print(light_factory.create_button().description)
    print(dark_factory.create_textbox().description)
    """
    OUTPUT:
    White button
    Black textbox
    """
