from abc import ABC, abstractmethod


class Computer:
    def __init__(self) -> None:
        self.cpu: str = ""
        self.memory: str = ""
        self.gpu: str = ""

    def __str__(self) -> str:
        return f"{self.cpu}, {self.memory}, {self.gpu}"


class ComputerBuilder(ABC):
    def __init__(self) -> None:
        self.computer: Computer = Computer()

    @abstractmethod
    def add_cpu(self) -> None:
        pass

    @abstractmethod
    def add_memory(self) -> None:
        pass

    @abstractmethod
    def add_gpu(self) -> None:
        pass


class GamingComputerBuilder(ComputerBuilder):
    def add_cpu(self):
        self.computer.cpu = "Intel i7 CPU"

    def add_memory(self):
        self.computer.memory = "RAM 16GB"

    def add_gpu(self):
        self.computer.gpu = "GTX"


class BasicComputerBuilder(ComputerBuilder):
    def add_cpu(self):
        self.computer.cpu = "Intel i5 CPU"

    def add_memory(self):
        self.computer.memory = "RAM 8GB"

    def add_gpu(self):
        self.computer.gpu = "no GPU"


class Director:
    def construct_computer(self, builder: ComputerBuilder):
        builder.add_cpu()
        builder.add_memory()
        builder.add_gpu()
        print(builder.computer)


if __name__ == "__main__":
    director = Director()
    basic = BasicComputerBuilder()
    gaming = GamingComputerBuilder()

    director.construct_computer(basic)
    director.construct_computer(gaming)
    """
    OUTPUT:
    Intel i5 CPU, RAM 8GB, no GPU
    Intel i7 CPU, RAM 16GB, GTX
    """
