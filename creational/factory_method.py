from abc import ABC, abstractmethod


class Transport(ABC):
    @abstractmethod
    def deliver(self) -> None:
        pass


class Ship(Transport):
    def deliver(self) -> None:
        print("Delivering by ship")


class Truck(Transport):
    def deliver(self) -> None:
        print("Delivering by truck")


class Logistics(ABC):
    @abstractmethod
    def create_transport(self) -> Transport:
        pass


class RoadLogistics(Logistics):
    def create_transport(self) -> Transport:
        return Truck()


class SeaLogistics(Logistics):
    def create_transport(self) -> Transport:
        return Ship()


if __name__ == "__main__":
    road_l = RoadLogistics()
    truck = road_l.create_transport()
    truck.deliver()

    sea_l = SeaLogistics()
    ship = sea_l.create_transport()
    ship.deliver()
    """
    OUTPUT:
    Delivering by truck
    Delivering by ship
    """
