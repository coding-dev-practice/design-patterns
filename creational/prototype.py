from __future__ import annotations
import copy


class Shape:
    def clone(self) -> Shape:
        return copy.deepcopy(self)


class Circle(Shape):
    def __init__(self, r: int) -> None:
        self.radius = r

    def __str__(self) -> str:
        return f"Circle with radius {self.radius}"


class Square(Shape):
    def __init__(self, a: int) -> None:
        self.a = a

    def __str__(self) -> str:
        return f"Square with side {self.a}"


if __name__ == "__main__":
    original_circle = Circle(5)
    cloned_circle = original_circle.clone()
    print(original_circle)
    print(cloned_circle)

    original_square = Square(8)
    cloned_square = original_square.clone()
    print(original_square)
    print(cloned_square)
    """
    OUTPUT:
    Circle with radius 5
    Circle with radius 5
    Square with side 8
    Square with side 8
    """
