from __future__ import annotations
from abc import ABC, abstractmethod
from typing import Optional


class Handler(ABC):
    def __init__(self, next_handler: Optional[Handler] = None) -> None:
        self.next_handler: Optional[Handler] = next_handler

    @abstractmethod
    def handle(self, request) -> None:
        print(f"Current state: {request}")
        if self.next_handler is not None:
            self.next_handler.handle(request)


# bits -> hex (str)
class BitsHandler(Handler):
    def handle(self, request: bytes) -> None:
        processed = request.decode()
        super().handle(processed)


# hex (str) -> int
class HexHandler(Handler):
    def handle(self, request: str) -> None:
        processed = int(request, base=16)
        super().handle(processed)


# int -> bool
class IntHandler(Handler):
    def handle(self, request: int) -> None:
        processed = request > 20
        super().handle(processed)


if __name__ == "__main__":
    chain = BitsHandler(HexHandler(IntHandler()))
    chain.handle(b"A1")
    print("=============")
    chain.handle(b"00")
    """
    OUTPUT:
    Current state: A1
    Current state: 161
    Current state: True
    =============
    Current state: 00
    Current state: 0
    Current state: False
    """
