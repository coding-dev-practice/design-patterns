from __future__ import annotations
from abc import ABC, abstractmethod


class AudioPlayer:
    def __init__(self) -> None:
        self.state: State = Ready(self)
        self.playing: bool = False

    def change_state(self, new_state: State) -> None:
        print(f"Switching to state {new_state}")
        self.state = new_state

    def button_lock(self) -> None:
        print("Clicked the button LOCK")
        self.state.click_lock()

    def button_play(self) -> None:
        print("Clicked the button PLAY")
        self.state.click_play()

    def button_next(self) -> None:
        print("Clicked the button NEXT")
        self.state.click_next()

    def button_previous(self) -> None:
        print("Clicked the button PREVIOUS")
        self.state.click_previous()

    def next_song(self) -> None:
        print("Switching to next song")

    def previous_song(self) -> None:
        print("Switching to previous song")

    def start_playing(self) -> None:
        self.playing = True
        print("Started playing the song")

    def stop_playing(self) -> None:
        self.playing = False
        print("Stopped playing the song")

    def lock(self) -> None:
        print("Player locked")

    def unlock(self) -> None:
        print("Player unlocked")


class State(ABC):
    def __init__(self, player: AudioPlayer) -> None:
        self.player: AudioPlayer = player

    @abstractmethod
    def click_lock(self) -> None:
        pass

    @abstractmethod
    def click_play(self) -> None:
        pass

    @abstractmethod
    def click_next(self) -> None:
        pass

    @abstractmethod
    def click_previous(self) -> None:
        pass


class Ready(State):
    def click_lock(self) -> None:
        self.player.change_state(Locked(self.player))

    def click_play(self) -> None:
        self.player.start_playing()
        self.player.change_state(Playing(self.player))

    def click_next(self) -> None:
        self.player.next_song()

    def click_previous(self) -> None:
        self.player.previous_song()

    def __str__(self) -> str:
        return "Ready"


class Locked(State):
    def click_lock(self) -> None:
        if self.player.playing:
            self.player.change_state(Playing(self.player))
        else:
            self.player.change_state(Ready(self.player))

    def click_play(self) -> None:
        print("Cannot play when locked")

    def click_next(self) -> None:
        print("Cannot move to next song when locked")

    def click_previous(self) -> None:
        print("Cannot move to previous song when locked")

    def __str__(self) -> str:
        return "Locked"


class Playing(State):
    def click_lock(self) -> None:
        self.player.change_state(Locked(self.player))

    def click_play(self) -> None:
        self.player.stop_playing()
        self.player.change_state(Ready(self.player))

    def click_next(self) -> None:
        self.player.next_song()

    def click_previous(self) -> None:
        self.player.previous_song()

    def __str__(self) -> str:
        return "Playing"


if __name__ == "__main__":
    audioplayer = AudioPlayer()
    audioplayer.button_play()
    audioplayer.button_lock()
    audioplayer.button_play()
    audioplayer.button_lock()
    audioplayer.button_next()
    """
    OUTPUT:
    Clicked the button PLAY
    Started playing the song
    Switching to state Playing
    Clicked the button LOCK
    Switching to state Locked
    Clicked the button PLAY
    Cannot play when locked
    Clicked the button LOCK
    Switching to state Playing
    Clicked the button NEXT
    Switching to next song
    """
