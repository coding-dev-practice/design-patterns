from __future__ import annotations
from abc import ABC, abstractmethod


class Observer(ABC):
    @abstractmethod
    def update(self, observable: Observable, *args):
        pass


class Observable:
    def __init__(self):
        self.__observers: list[Observer] = []

    def add_observer(self, observer: Observer):
        self.__observers.append(observer)

    def delete_observer(self, observer: Observer):
        self.__observers.remove(observer)

    def notify_observers(self, *args):
        for observer in self.__observers:
            observer.update(self, *args)


class Twitter(Observable, Observer):
    def __init__(self, name: str):
        super().__init__()
        self.name: str = name

    def tweet(self, message: str):
        self.notify_observers(self, message)

    def follow(self, other_twit: Twitter):
        other_twit.add_observer(self)
        return self

    def update(self, observable: Twitter, *args):
        print(f"{self.name} received a tweet from {observable.name}: {args[1]}")


if __name__ == "__main__":
    a = Twitter("Alice")
    k = Twitter("King")
    q = Twitter("Queen")
    h = Twitter("Mad Hatter")
    c = Twitter("Cheshire Cat")

    a.follow(c).follow(h).follow(q)
    k.follow(q)
    q.follow(q).follow(h)
    h.follow(a).follow(q).follow(c)

    print(f"==== {q.name} tweets ====")
    q.tweet("Off with their heads!")
    print(f"\n==== {a.name} tweets ====")
    a.tweet("What a strange world we live in.")
    print(f"\n==== {k.name} tweets ====")
    k.tweet("Begin at the beginning, and go on till you come to the end: then stop.")
    print(f"\n==== {c.name} tweets ====")
    c.tweet("We're all mad here.")
    print(f"\n==== {h.name} tweets ====")
    h.tweet("Why is a raven like a writing-desk?")

    """
    OUTPUT:
    ==== Queen tweets ====
    Alice received a tweet from Queen: Off with their heads!
    King received a tweet from Queen: Off with their heads!
    Queen received a tweet from Queen: Off with their heads!
    Mad Hatter received a tweet from Queen: Off with their heads!

    ==== Alice tweets ====
    Mad Hatter received a tweet from Alice: What a strange world we live in.

    ==== King tweets ====

    ==== Cheshire Cat tweets ====
    Alice received a tweet from Cheshire Cat: We're all mad here.
    Mad Hatter received a tweet from Cheshire Cat: We're all mad here.

    ==== Mad Hatter tweets ====
    Alice received a tweet from Mad Hatter: Why is a raven like a writing-desk?
    Queen received a tweet from Mad Hatter: Why is a raven like a writing-desk?
    """
