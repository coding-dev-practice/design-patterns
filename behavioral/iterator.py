from abc import ABC, abstractmethod


class Car:
    def __init__(self, name: str) -> None:
        self.name: str = name


class Iterator(ABC):
    @abstractmethod
    def get_next(self) -> Car:
        pass

    @abstractmethod
    def has_more(self) -> bool:
        pass


class FerrariIterator(Iterator):
    def __init__(self, cars: list[Car]) -> None:
        self.data: list[Car] = cars
        self.it: int = -1

    def get_next(self) -> Car:
        if self.has_more():
            self.it += 1
            return self.data[self.it]

    def has_more(self) -> bool:
        if self.it < len(self.data) - 1:
            return True
        return False


class FordIterator(Iterator):
    def __init__(self, cars: dict[Car]) -> None:
        self.data: dict[Car] = cars
        self.keys: list[str] = list(self.data.keys())

    def get_next(self) -> Car:
        if self.has_more():
            key = self.keys.pop(0)
            return self.data[key]

    def has_more(self) -> bool:
        if len(self.keys) > 0:
            return True
        return False


class Company(ABC):
    @abstractmethod
    def add_car(self, car: Car) -> None:
        pass

    @abstractmethod
    def create_iterator(self) -> Iterator:
        pass


class Ferrari(Company):
    def __init__(self) -> None:
        self.cars: list[Car] = []

    def add_car(self, car: Car) -> None:
        self.cars.append(car)

    def create_iterator(self) -> Iterator:
        return FerrariIterator(self.cars)


class Ford(Company):
    def __init__(self) -> None:
        self.cars: dict[Car] = {}

    def add_car(self, car: Car) -> None:
        self.cars[car.name] = car

    def create_iterator(self) -> Iterator:
        return FordIterator(self.cars)


class Dealer:
    def __init__(self, companies: list[Company]) -> None:
        self.companies: list[Company] = companies

    def print_cars(self) -> None:
        for company in self.companies:
            self._print_company_cars(company.create_iterator())
            print("==============")

    def _print_company_cars(self, iterator: Iterator) -> None:
        while iterator.has_more():
            print(iterator.get_next().name)


if __name__ == "__main__":
    ferrari = Ferrari()
    ferrari.add_car(Car("Ferrari F40"))
    ferrari.add_car(Car("Ferrari F355"))
    ferrari.add_car(Car("Ferrari 125 S"))
    ferrari.add_car(Car("Ferrari Ultra"))

    ford = Ford()
    ford.add_car(Car("Ford Model T"))
    ford.add_car(Car("Ford Focus"))
    ford.add_car(Car("Ford Escort"))
    ford.add_car(Car("Ford Ultra"))

    dealer = Dealer([ferrari, ford])
    dealer.print_cars()
    """
    OUTPUT:
    Ferrari F40
    Ferrari F355
    Ferrari 125 S
    Ferrari Ultra
    ==============
    Ford Model T
    Ford Focus
    Ford Escort
    Ford Ultra
    ==============
    """
