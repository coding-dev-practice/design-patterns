from __future__ import annotations


class Snapshot:
    def __init__(
        self, editor: Editor, text: str, x: int, y: int, selection_width: int
    ) -> None:
        self.editor: Editor = editor
        self.saved_text: str = text
        self.saved_x: int = x
        self.saved_y: int = y
        self.saved_selection_width: int = selection_width

    def restore(self) -> None:
        self.editor.set_text(self.saved_text)
        self.editor.set_cursor(self.saved_x, self.saved_y)
        self.editor.set_selection_width(self.saved_selection_width)


class Editor:
    def __init__(self) -> None:
        self.text: str = "abcdef"
        self.x: int = 0
        self.y: int = 0
        self.selection_width: int = 6

    def set_text(self, text: str) -> None:
        self.text = text

    def set_cursor(self, x: int, y: int) -> None:
        self.x = x
        self.y = y

    def set_selection_width(self, selection_width: int) -> None:
        self.selection_width = selection_width

    def save_state(self) -> Snapshot:
        return Snapshot(self, self.text, self.x, self.y, self.selection_width)

    def show_state(self) -> None:
        print(
            f"Text: {self.text}, cursor: ({self.x}, {self.y}), selection width: {self.selection_width}"
        )


if __name__ == "__main__":
    editor = Editor()
    editor.show_state()
    snapshot = editor.save_state()

    editor.set_text("I like tea")
    editor.set_cursor(3, 9)
    editor.set_selection_width(10)
    editor.show_state()

    snapshot.restore()
    editor.show_state()
    """
    OUTPUT:
    Text: abcdef, cursor: (0, 0), selection width: 6
    Text: I like tea, cursor: (3, 9), selection width: 10
    Text: abcdef, cursor: (0, 0), selection width: 6
    """
