from __future__ import annotations
from abc import ABC, abstractmethod


class Item(ABC):
    @abstractmethod
    def accept(self, visitor: Visitor):
        pass


class Visitor(ABC):
    @abstractmethod
    def visit_book(self, book: Book) -> int:
        pass

    @abstractmethod
    def visit_fruit(self, fruit: Fruit) -> int:
        pass


class Book(Item):
    def __init__(self, price: int, isbn: str) -> None:
        self.price: int = price
        self.isbn: str = isbn

    def get_price(self) -> int:
        return self.price

    def get_isbn_number(self) -> str:
        return self.isbn

    def accept(self, visitor: Visitor):
        return visitor.visit_book(self)


class Fruit(Item):
    def __init__(self, price_kg: int, weight: int, name: str) -> None:
        self.price_kg: int = price_kg
        self.weight: int = weight
        self.name: str = name

    def get_price_kg(self) -> int:
        return self.price_kg

    def get_weight(self) -> int:
        return self.weight

    def get_name(self) -> str:
        return self.name

    def accept(self, visitor: Visitor):
        return visitor.visit_fruit(self)


class ShoppingCartVisitor(Visitor):
    def visit_book(self, book: Book) -> int:
        cost: int = book.get_price()
        if cost > 50:
            cost -= 5  # discount
        print(f"Book isbn={book.get_isbn_number()} costs ${cost}")
        return cost

    def visit_fruit(self, fruit: Fruit) -> int:
        cost: int = fruit.get_price_kg() * fruit.get_weight()
        print(f"{fruit.name} costs ${cost}")
        return cost


items: list[Item] = [
    Book(20, "1234"),
    Book(60, "5678"),
    Fruit(5, 1, "banana"),
    Book(10, "9876"),
    Fruit(10, 10, "apple"),
]
shopping_cart_visitor = ShoppingCartVisitor()
total_cost: int = 0
for item in items:
    total_cost += item.accept(shopping_cart_visitor)
print(f"Total cost: ${total_cost}")
