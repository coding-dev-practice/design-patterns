from abc import ABC, abstractmethod
from typing import TextIO


class AverageCalculator(ABC):
    def average(self) -> float:
        try:
            num_items = 0
            total_sum = 0
            while self.has_next():
                total_sum += self.next_item()
                num_items += 1
            if num_items == 0:
                raise RuntimeError("Can't compute the average of zero items.")
            return total_sum / num_items
        finally:
            self.dispose()

    @abstractmethod
    def has_next(self) -> bool:
        pass

    @abstractmethod
    def next_item(self) -> float:
        pass

    def dispose(self) -> None:
        pass


class FileAverageCalculator(AverageCalculator):
    def __init__(self, file: TextIO):
        self.file = file
        self.last_line = self.file.readline()

    def has_next(self) -> bool:
        return self.last_line != ""

    def next_item(self) -> float:
        result = float(self.last_line)
        self.last_line = self.file.readline()
        return result

    def dispose(self) -> None:
        self.file.close()


class MemoryAverageCalculator(AverageCalculator):
    def __init__(self, num_list: list[float]) -> None:
        self.num_list = num_list
        self.id = -1

    def has_next(self) -> bool:
        if self.id < len(self.num_list) - 1:
            return True
        return False

    def next_item(self) -> float:
        self.id += 1
        return self.num_list[self.id]

    def dispose(self) -> None:
        pass


if __name__ == "__main__":
    fac = FileAverageCalculator(open("data.txt"))
    print(fac.average())

    mac = MemoryAverageCalculator([3, 1, 4, 1, 5, 9, 2, 6, 5, 3])
    print(mac.average())
    """
    OUTPUT:
    18.0
    3.9
    """
