from abc import ABC, abstractmethod
from typing import Optional


class Command(ABC):
    @abstractmethod
    def execute(self) -> None:
        pass


class Light:
    def turn_on(self) -> None:
        print("Light turned on")

    def turn_off(self) -> None:
        print("Light turned off")


class TurnOnCommand(Command):
    def __init__(self, light: Light) -> None:
        self.light = light

    def execute(self) -> None:
        self.light.turn_on()


class TurnOffCommand(Command):
    def __init__(self, light: Light) -> None:
        self.light = light

    def execute(self) -> None:
        self.light.turn_off()


class RemoteControl:
    def __init__(self) -> None:
        self.command: Optional[Command] = None

    def set_command(self, command) -> None:
        self.command = command

    def press_button(self) -> None:
        self.command.execute()


if __name__ == "__main__":
    light = Light()
    remote = RemoteControl()
    remote.set_command(TurnOnCommand(light))
    remote.press_button()

    remote.set_command(TurnOffCommand(light))
    remote.press_button()
    """
    OUTPUT:
    Light turned on
    Light turned off
    """
