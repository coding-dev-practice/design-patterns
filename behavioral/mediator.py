from __future__ import annotations
from abc import ABC, abstractmethod


class User:
    def __init__(self, name: str, chat_mediator: ChatMediator) -> None:
        self.name = name
        self.chat_mediator = chat_mediator

    def send(self, message: str) -> None:
        self.chat_mediator.send_message(message, self)

    def receive_message(self, message: str) -> None:
        print(f"{self.name} received a message: {message}")


class ChatMediator(ABC):
    @abstractmethod
    def add_user(self, user: User) -> None:
        pass

    @abstractmethod
    def send_message(self, message: str, user: User) -> None:
        pass


class ChatRoom(ChatMediator):
    def __init__(self) -> None:
        self.users: list[User] = []

    def add_user(self, user: User) -> None:
        self.users.append(user)

    def send_message(self, message: str, user: User) -> None:
        print(f"User {user.name} is sending a message {message}")
        for other_user in self.users:
            if other_user != user:
                other_user.receive_message(message)
        print("================")


if __name__ == "__main__":
    chat_room = ChatRoom()
    alice = User("Alice", chat_room)
    john = User("John", chat_room)
    kate = User("Kate", chat_room)
    chat_room.add_user(alice)
    chat_room.add_user(john)
    chat_room.add_user(kate)

    alice.send("Hi hi!")
    john.send("Hello Alice!")
    kate.send("Nice to meet you.")
    """
    OUTPUT:
    User Alice is sending a message Hi hi!
    John received a message: Hi hi!
    Kate received a message: Hi hi!
    ================
    User John is sending a message Hello Alice!
    Alice received a message: Hello Alice!
    Kate received a message: Hello Alice!
    ================
    User Kate is sending a message Nice to meet you.
    Alice received a message: Nice to meet you.
    John received a message: Nice to meet you.
    ================
    """
