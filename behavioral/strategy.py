from abc import ABC, abstractmethod


class Strategy(ABC):
    @abstractmethod
    def calculate_route(self) -> int:
        pass


class RoadStrategy(Strategy):
    def calculate_route(self) -> int:
        return 5


class WalkingStrategy(Strategy):
    def calculate_route(self) -> int:
        return 20


class Navigator:
    def __init__(self, strategy: Strategy) -> None:
        self.strategy = strategy

    def calculate_route(self) -> int:
        return self.strategy.calculate_route()


if __name__ == "__main__":
    nav = Navigator(WalkingStrategy())
    print(nav.calculate_route())
    nav.strategy = RoadStrategy()
    print(nav.calculate_route())
    """
    OUTPUT:
    20
    5
    """
