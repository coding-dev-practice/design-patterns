# Design patterns


This repository contains examples of simple implementations of design patterns in Python. The inspirations (or the exact examples) are taken from Refactoring Guru, ChatGPT and other resources found on the internet.
